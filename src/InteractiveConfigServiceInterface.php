<?php

declare(strict_types=1);

namespace App\RpcService;


interface InteractiveConfigServiceInterface
{
    /**
     * 获取vip价格
     * @param int $vip_id vip_id
     * @return mixed
     */
    public function getPriceByVipId(int $vip_id);

    /**
     * 获取vip天数
     * @param int $vip_id
     * @return mixed
     */
    public function getDayByVipId(int $vip_id);


    /**
     * 获取互动配置
     * @param string $key key
     */
    public function getFreetimeConfig(string $key);

    /**
     * 获取霸屏次数价格
     * @param int $barrage_price_id 价格id
     * @return mixed
     */
    public function getPriceByBarragePriceId(int $barrage_price_id);

    /**
     * 获取霸屏次数
     * @param int $barrage_price_id 价格id
     * @return mixed
     */
    public function getTimesByBarragePriceId(int $barrage_price_id);
}