<?php

declare(strict_types=1);

namespace YinChuang\RpcService;


interface UserInfoServiceInterface
{
    /**
     * 根据UserId获取用户信息
     * @param int $user_id 用户id
     * @return array
     */
    public function getByUserId(int $user_id): array;

    /**
     * 根据openId获取用户信息
     * @param string $openId
     * @return array
     */
    public function getByOpenId(string $openId): array;

    /**
     * 根据unionId获取用户信息
     * @param string $unionId
     * @return array
     */
    public function getByUnionId(string $unionId): array;

    /**
     * @param int $user_id
     * @param array $data
     * @return mixed
     */
    public function updateUserByUserId(int $user_id,array $data);
}