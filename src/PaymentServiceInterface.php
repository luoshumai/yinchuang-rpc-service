<?php

declare(strict_types=1);

namespace App\RpcService;


interface PaymentServiceInterface
{
    /**
     * 支付
     * @param string $body 商品描述
     * @param string $outTradeNo 商户订单号
     * @param float $totalFee 支付金额
     * @param string $notifyUrl 通知地址
     * @param string $openId 用户标识
     * @param string $channel 渠道
     * @param string $tradeType 交易类型
     * @return mixed
     */
    public function pay($body, $outTradeNo, $totalFee, $notifyUrl, $openId, $channel, $tradeType = 'JSAPI');

    /**
     * 根据微信订单号退款
     * @param string $channel
     * @param string $transactionId 微信支付订单号
     * @param string $refundNumber 商户退款单号
     * @param float$totalFee 订单金额
     * @param float $refundFee 订单金额
     * @return mixed
     */
    public function refundByTransactionId($channel, $transactionId, $refundNumber, $totalFee, $refundFee);

    /**
     * 根据商户订单号退款
     * @param string $channel
     * @param string $outTradeNo 微信支付订单号
     * @param string $refundNumber 商户退款单号
     * @param float $totalFee 订单金额
     * @param float $refundFee 订单金额
     * @return mixed
     */
    public function refundByOutTradeNumber($channel, $outTradeNo, $refundNumber, $totalFee, $refundFee);

    /**
     * 企业付款到零钱
     * @param string $channel
     * @param string $partnerTradeNo 商户订单号
     * @param string $openId 用户openid
     * @param string $checkName 校验用户姓名选项 => NO_CHECK：不校验真实姓名;FORCE_CHECK：强校验真实姓名
     * @param string $reUserName 收款用户姓名
     * @param float $amount 金额
     * @param string $desc 企业付款备注
     * @return mixed
     */
    public function transferToBalance($channel, $partnerTradeNo, $openId, $checkName = 'NO_CHECK', $reUserName = '', $amount, $desc = '');
}